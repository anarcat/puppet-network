# common resources for network management
class network {
  # ensure interfaces.d is sourced
  file_line {
    'interfaces.d':
      path   => '/etc/network/interfaces',
      line   => 'source /etc/network/interfaces.d/*',
      match  => '^source(-directory)?\s+/etc/network/interfaces.d(/\*)?$',
      notify => Exec['network-reload'];
  }
  exec {
    'network-reload':
      command     => 'service networking reload',
      refreshonly => true;
  }
}
