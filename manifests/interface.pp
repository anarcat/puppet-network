# opiniated network interface definition
#
# this currently just adds an address to an interface and reloads the
# network. it only works on Debian.
define network::interface(
  Optional[String[1]] $source  = undef,
  Optional[String[1]] $content = undef,
) {
  include network

  file {
    "/etc/network/interfaces.d/${name}":
      ensure  => present,
      require => File_line['interfaces.d'],
      notify  => Exec['network-reload'],
      source  => $source,
      content => $content;
  }
}
