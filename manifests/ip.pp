# define an additional IP address for this interface
define network::ip(
  Stdlib::IP::Address $address,
  Optional[String[1]] $iface_name = $name,
) {
  network::interface { $name:
    content => @("EOF");
      iface ${iface_name} inet static
          address ${address}
      | EOF
  }
}
